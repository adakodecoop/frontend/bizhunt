import { ApiClient, AuthService } from '~/services';

export default ({ $strapi }, inject) => {
  const apiClient = ApiClient.init({ client: $strapi });
  inject('authService', AuthService(apiClient));
};
