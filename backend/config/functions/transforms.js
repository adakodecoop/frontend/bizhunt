'use strict';

const { normalizeURL } = require('./urls');

/** */
const transformBizcard = (bizcard) => {
  const details = bizcard.details[0];

  bizcard.details = [
    {
      ...details,
      attachments: details.attachments?.map((a) => ({ ...a, url: normalizeURL(a.url) })),
    }
  ];

  return bizcard;
};

/** */
const transforms = {
  'bizcard': transformBizcard,
};

module.exports = {
  /** */
  transformEntity: (entity, entityName) => transforms[entityName](entity),
};