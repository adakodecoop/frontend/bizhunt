export default `
  query getBizcards {
    bizcards {
      id
      slug
      title
      source
      date
      topics {
        code
        name
        color
      }
      type {
        code
        name
        color
      }
      geofocus {
        code
        name
      }
    }
  }
`;
