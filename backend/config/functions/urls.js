'use strict';

const { $URL, joinURL } = require('ufo');

const apiURL = process.env.API_URL;

module.exports = {
  /** */
  normalizeURL(url, host = apiURL) {
    const _url = new $URL(url);

    /* eslint-disable-next-line no-extra-boolean-cast */
    if (!Boolean(_url.host)) {
      return joinURL(host, url);
    }

    return url;
  },
};
