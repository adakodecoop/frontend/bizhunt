const topics = require('../data/topics.json');

exports.seed = async (knex) => {
  // Delete ALL existing entries
  await knex('topics').truncate();
  // Insert seed entries
  await knex('topics').insert(topics);
};
