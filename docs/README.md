# Biz Hunt Documentation

Biz Hunt's documentation.

## Contents

1. [Infraestructura](content/01-infraestructura.md)
2. [Informació tècnica](content/02-informacio-tecnica.md)
3. [Recursos](content/03-recursos.md)

## Contributors

Maintained by

<!-- prettier-ignore-start -->

| [<img alt="laklau" src="https://secure.gravatar.com/avatar/15c5fa5af66362d8b756c2458a06821c?s=117&d=identicon" width="117">](https://gitlab.com/adakodecoop/frontend/bizhunt/-/graphs/main) | [<img alt="zuzudev" src="https://secure.gravatar.com/avatar/f925124b0eed1eeab8513016914b9150?s=117&d=identicon" width="117">](https://gitlab.com/adakodecoop/frontend/bizhunt/-/graphs/main) |
| :---: | :---: |
| [Klaudia Alvarez](https://github.com/laklau) | [Carles Muiños](https://github.com/zuzust) |

<!-- prettier-ignore-end -->

## Contact

Email: hola[@]adakode[.]org  
Twitter: [@adakodecoop](https://twitter.com/adakodecoop)  
Facebook: [adakodecoop](https://www.facebook.com/adakodecoop)  
LinkedIn: [adakodecoop](https://www.linkedin.com/company/adakodecoop)

## License

The contents of this repository is &copy; 2020-present [Adakode](https://adakode.org) under the terms of the [MIT-Hippocratic License](https://firstdonoharm.dev/version/2/1/license.html).
