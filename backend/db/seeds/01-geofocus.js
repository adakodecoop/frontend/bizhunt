const geofocus = require('../data/geofocus.json');

exports.seed = async (knex) => {
  // Delete ALL existing entries
  await knex('geofocus').truncate();
  // Insert seed entries
  await knex('geofocus').insert(geofocus);
};
