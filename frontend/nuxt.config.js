export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Biz Hunt by Citilab',
    htmlAttrs: {
      lang: 'ca',
    },
    meta: [
      { hid: 'charset', charset: 'utf-8' },
      {
        hid: 'viewport',
        name: 'viewport',
        content: 'width=device-width, initial-scale=1',
      },
      {
        hid: 'description',
        name: 'description',
        content: 'Biz Hunt: Find appealing business oportunities',
      },
    ],
    link: [
      { hid: 'icon', rel: 'icon', type: 'image/png', href: '/favicon.png' },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    // '~/plugins/msw.client.js',
    '~/plugins/app-messages.js',
    '~/plugins/auth-service.js',
    '~/plugins/data-service.js',
    '~/plugins/tw-config.js',
    '~/plugins/vue-formulate.js',
    '~/plugins/v-click-outside.js',
    '~/plugins/ss-select.js',
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [
    { path: '~/components/', extensions: ['vue'], pathPrefix: false },
  ],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // Doc: https://pwa.nuxtjs.org/
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    // Doc: https://composition-api.nuxtjs.org/getting-started/introduction
    '@nuxtjs/composition-api/module',
    // Doc: https://google-fonts.nuxtjs.org/
    '@nuxtjs/google-fonts',
    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    '@nuxtjs/tailwindcss',
    // Doc: https://github.com/nuxt-community/svg-module
    '@nuxtjs/svg',
    // Doc: https://github.com/nuxt-community/svg-sprite-module
    '@nuxtjs/svg-sprite',
    // Doc: https://github.com/teamnovu/nuxt-breaky
    // '@teamnovu/nuxt-breaky',
  ],

  // PWA module: https://pwa.nuxtjs.org/setup
  pwa: {
    meta: {
      name: 'Biz Hunt by Citilab',
      author: '@citilab',
      description: 'Find appealing business oportunities',
      theme_color: '#b43403', // bg-orange-700
      lang: 'ca',
    },
    manifest: {
      name: 'Biz Hunt by Citilab',
      short_name: 'Biz Hunt',
      description: 'Find appealing business oportunities',
      background_color: '#b43403', // bg-orange-700
      lang: 'ca',
    },
    workbox: {
      cacheNames: {
        prefix: 'bizhunt',
        suffix: 'v1',
      },
      runtimeCaching: [
        /* prettier-ignore */
        // Fonts
        {
          urlPattern: 'https://fonts.googleapis.com/|https://fonts.gstatic.com/',
          handler: 'StaleWhileRevalidate',
          strategyOptions: {
            cacheName: 'bizhunt-fonts-v1',
          },
          strategyPlugins: [
            {
              use: 'Expiration',
              config: {
                maxEntries: 30,
              }
            }
          ],
        },
        // Uploads
        {
          urlPattern: `${process.env.API_URL}/uploads/`,
          handler: 'StaleWhileRevalidate',
          strategyOptions: {
            cacheName: 'bizhunt-uploads-v1',
          },
          strategyPlugins: [
            {
              use: 'Expiration',
              config: {
                maxEntries: 90,
              },
            },
          ],
        },
      ],
    },
  },

  // Google Fonts module: https://google-fonts.nuxtjs.org/options
  googleFonts: {
    families: {
      Inter: {
        wght: [300, 400, 500, 700],
      },
    },
    display: 'swap',
  },

  // Tailwind CSS module: https://tailwindcss.nuxtjs.org/options
  tailwindcss: {
    cssPath: '~/assets/styles/index.css',
    exposeConfig: true,
  },

  // SVG Sprite module
  svgSprite: {
    input: '~/assets/icons/svg',
    output: '~/assets/icons/sprites',
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // Doc: https://github.com/nuxt-community/strapi-module
    '@nuxtjs/strapi',
    // Doc: https://github.com/nuxt-community/modules/tree/master/packages/toast
    '@nuxtjs/toast',
  ],

  // Toast module
  toast: {
    position: 'bottom-left',
    duration: 5000,
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  // Router: https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-router
  router: {},

  // Loading Progress Bar: https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-loading
  loading: false,

  // Runtime Config: https://nuxtjs.org/docs/2.x/directory-structure/nuxt-config#runtimeconfig
  publicRuntimeConfig: {
    appURL: process.env.APP_URL || 'http://localhost:3000',
    apiURL: process.env.API_URL || 'http://localhost:1337',
  },
  privateRuntimeConfig: {},

  // Vue Config: https://nuxtjs.org/docs/2.x/configuration-glossary/configuration-vue-config
  vue: {
    config: {
      devtools: process.env.NODE_ENV !== 'production',
    },
  },
};
