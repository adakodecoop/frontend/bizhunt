import { Meta, Preview, Props, Story } from '@storybook/addon-docs/blocks';
import { DependenciesTable } from 'storybook-addon-deps/blocks';
import { withKnobs, object } from '@storybook/addon-knobs';

import ABoxL from './ABoxL.vue';

<Meta
  title="Core/Layouts/ABoxL"
  component={ABoxL}
  parameters={{ component: ABoxL }}
/>

# A-Box-L

Takes care of any styles that can be considered intrinsic to individual elements.

<Preview>
  <Story name="default" decorators={[withKnobs]}>
    {{
      components: { ABoxL },
      props: {
        styles: {
          default: () => object(
            'CSS Variables',
            {
              '--a-box-l--p': '1.5rem',
              '--a-box-l--text-color': 'inherit',
              '--a-box-l--bg-color': 'inherit',
              '--a-box-l--border': '0',
              '--a-box-l--border-color': 'inherit',
            },
            'Styles'
          ),
        },
      },
      template: `
        <a-box-l :style="styles">
          <span>The default box</span>
        </a-box-l>
      `,
    }}
  </Story>
  <Story name="withBorder">
    {{
      components: { ABoxL },
      template: `
        <a-box-l
          :style="{
            '--a-box-l--border': '1px',
            '--a-box-l--border-color': '#f17eb8',
          }">
          <span>A box with border-pink-500</span>
        </a-box-l>
      `,
    }}
  </Story>
  <Story name="withColors">
    {{
      components: { ABoxL },
      template: `
        <a-box-l
          :style="{
            '--a-box-l--text-color': '#0e9f6e',
            '--a-box-l--bg-color': '#f1f5f9',
          }">
          <span>A box with text-green-500 and bg-cool-gray-100</span>
        </a-box-l>
      `,
    }}
  </Story>
  <Story name="noPadding">
    {{
      components: { ABoxL },
      template: `
        <a-box-l
          :style="{
            '--a-box-l--p': '0',
            '--a-box-l--bg-color': '#f1f5f9',
          }">
          <span>A box with no padding and bg-cool-gray-100</span>
        </a-box-l>
      `,
    }}
  </Story>
</Preview>

## API

<Props of={ABoxL} />

## Installation

```bash
yarn add -E @bit/adakode.ak-ui.core.layouts.a-box
```

## Usage

> The basic, and highly prolific, use case for a Box is to group and demarcate
> some content. This content may appear as a message or 'note' among other
> textual flow content, as one card in a grid of many, or as the inner wrapper of a
> positioned dialog element.

```html
<!-- Basic Box -->
<a-box-l>
  <!-- contents of the box -->
</a-box-l>
```

```html
<!-- Box with a header -->
<a-box-l class="p-0">
  <a-box-l><!-- the box head --></a-box-l>
  <a-box-l><!-- the box body --></a-box-l>
</a-box-l>
```

## Customization

There are several ways to customize the look and feel of the component,
depending on the desired scope of the customization.

### Global Customization

To customize the look and feel of **every single instance** of the component
create a css file defining a class named after the kebab-case version of the component name
and define any of the custom properties below:

| CSS variable              |   default | design token             |
| :------------------------ | --------: | :----------------------- |
| `--a-box-l--p`            |  `1.5rem` | `theme('padding.6')`     |
| `--a-box-l--text-color`   | `inherit` | `n/a`                    |
| `--a-box-l--bg-color`     | `inherit` | `n/a`                    |
| `--a-box-l--border`       |       `0` | `theme('borderWidth.0')` |
| `--a-box-l--border-color` | `inherit` | `n/a`                    |

These are the steps:

```bash
mkdir -p src/assets/styles/components/ui/core/layouts && touch $_/ABoxL.css
```

```css
/* <rootDir>/src/assets/styles/components/index.css */
@import './ui/core/layouts/ABoxL.css';
```

```css
/* <rootDir>/src/assets/styles/components/ui/core/layouts/ABoxL.css */
.a-box-l {
  --a-box-l--p: theme('padding.6');
  --a-box-l--text-color: inherit;
  --a-box-l--bg-color: inherit;
  --a-box-l--border: theme('borderWidth.0');
  --a-box-l--border-color: inherit;
}
```

### Scoped Customization

To customize the look and feel of a **group of instances** of the component
augment the specificity of the component class with a scoping class:

```css
/* <rootDir>/src/assets/styles/components/ui/core/layouts/ABoxL.css */
.a-box-l.a-box-l--invert {
  --a-box-l--text-color: hsl(0, 0%, 20%); /* dark color */
  --a-box-l--bg-color: hsl(0, 0%, 80%); /* light color */

  filter: invert(100%);
}
```

```html
<!-- <rootDir>/src/components/path/to/the/ConsumerComponent.vue -->
<!-- A greyscale inverted Box -->
<a-box-l class="a-box-l--invert">
  <!-- contents of the box -->
</a-box-l>
```

### Instance Customization

To customize the look and feel of a **single instance** of the component
you can define the desired CSS variables inline:

```html
<!-- Using CSS variables -->
<a-box-l
  :style="{
    '--a-box-l--p': '1.5rem',
    '--a-box-l--text-color': '#252f3f',
    '--a-box-l--bg-color': '#f4f5f7',
    '--a-box-l--border': '0',
    '--a-box-l--border-color': '#252f3f',
  }"
>
  <!-- contents of the box -->
</a-box-l>
```

or use the proper utility classes after a `reset` class:

```html
<!-- Using utility classes -->
<a-box-l class="reset p-6 text-gray-800 bg-gray-100 border-0 border-gray-800">
  <!-- contents of the box -->
</a-box-l>
```

## Dependencies

<DependenciesTable
  titleDependencies="Dependencies"
  titleDependents="Dependents"
  of={ABoxL}
/>

## Links

Source: https://gitlab.com/adakodecoop/coop/adakode-ui/-/tree/master/src/components/ui/core/layouts/ABox  
Bit: https://bit.dev/adakode/ak-ui/core/layouts/a-box  
Every Layout: https://absolutely.every-layout.dev/layouts/box/
