// See: https://strapi.io/documentation/developer-docs/latest/guides/custom-admin.html#update-the-content-manager

import { dateFormats as defaultDateFormats } from 'strapi-helper-plugin';

const dateFormats = {
  ...defaultDateFormats,
  // Customise the format by uncommenting the one you wan to override it corresponds to the type of your field
  // date: 'dddd, MMMM Do YYYY',
  // datetime: 'dddd, MMMM Do YYYY HH:mm',
  // time: 'HH:mm A',
  // timestamp: 'dddd, MMMM Do YYYY HH:mm',
  datetime: 'DD/MM/YYYY HH:mm',
  timestamp: 'DD/MM/YYYY HH:mm',
};

export default dateFormats;
