export default (ctx, inject) => {
  // prettier-ignore
  const messages = {
    500: "Vaja! S'ha produït un error inesperat en servidor.",
    'login.error': 'Les credencials indicades no són vàlides.',
    'login.forgot.error': "L'adreça de correu indicada no està registrada.",
    'login.reset.error': "Comprova que l'enllaç a la pàgina contingui el codi de restauració enviat.",
    'topics.fetch.error': "Vaja! S'ha produït un error en carregar el filtre de topics de les bizcards.",
    'types.fetch.error': "Vaja! S'ha produït un error en carregar el filtre de tipus de les bizcards.",
    'geofocus.fetch.error': "Vaja! S'ha produït un error en carregar el filtre d'àmbit geogràfic de les bizcards.",
    'bizcards.fetch.error': "Vaja! S'ha produït un error en carregar les bizcards.",
    'bizcards.fetch.empty': 'No hi ha bizcards actives en aquest moment.',
    'bizcards.no-detail-selected': "← Selecciona una bizcard per veure'n els detalls o utilitza els filtres per mostrar resultats del teu interès.",
    'bizcards.filter.noresults': '← Aquesta cerca no ha donat cap resultat.',
    'bizcards.tryagain': 'Canvia els paràmetres dels filtres per obtenir-ne.',
    'bizcards.browse': 'Utilitza els filtres, segur que en trobes del teu interés.',
    'bizcard.fetch.error': "Vaja! S'ha produït un error en carregar el detall de la bizcard indicada.",
    'bizcard.fetch.unavailable': "Ooops! No hi ha cap bizcard activa que respongui a la url indicada.",
    'bizhunt.comeback-tomorrow': 'Torna demà, potser aleshores et podrem mostrar de noves.',
    'default': "Surt de l'aplicació i torna a entrar passats uns segons. Si el problema persisteix contacta amb el responsable de l'aplicació."
  };

  inject('appMessages', messages);
};
