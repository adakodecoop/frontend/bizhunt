# Biz Hunt Backend

Biz Hunt's backend Strapi app.

## Contributors

Designed, developed and maintained by

<!-- prettier-ignore-start -->

| [<img alt="laklau" src="https://secure.gravatar.com/avatar/15c5fa5af66362d8b756c2458a06821c?s=117&d=identicon" width="117">](https://gitlab.com/adakodecoop/frontend/bizhunt/-/graphs/main) | [<img alt="zuzudev" src="https://secure.gravatar.com/avatar/f925124b0eed1eeab8513016914b9150?s=117&d=identicon" width="117">](https://gitlab.com/adakodecoop/frontend/bizhunt/-/graphs/main) |
| :---: | :---: |
| [Klaudia Alvarez](https://github.com/laklau) | [Carles Muiños](https://github.com/zuzust) |

<!-- prettier-ignore-end -->

## Contact

Email: hola[@]adakode[.]org  
Twitter: [@adakodecoop](https://twitter.com/adakodecoop)  
Facebook: [adakodecoop](https://www.facebook.com/adakodecoop)  
LinkedIn: [adakodecoop](https://www.linkedin.com/company/adakodecoop)

## License

The code of this repository is &copy; 2020-present [Adakode](https://adakode.org) under the terms of the [MIT-Hippocratic License](https://firstdonoharm.dev/version/2/1/license.html).
