export default ({ app }) => {
  // console.log('[middleware/clear-credentials]');
  return app.$authService.logout();
};
