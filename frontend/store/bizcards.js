export const state = () => ({
  bizcards: [],
  selected: null,
});

export const mutations = {
  SET_BIZCARDS: (state, bizs) => (state.bizcards = bizs),
  SET_SELECTED_BIZCARD: (state, bizSlug) => (state.selected = bizSlug),
};

export const actions = {
  setBizcards: ({ commit }, bizs) => {
    commit('SET_BIZCARDS', bizs);
  },
  setSelectedBizcard: ({ commit }, bizSlug) => {
    commit('SET_SELECTED_BIZCARD', bizSlug);
  },
};

export const getters = {
  getBizcards: (state) => state.bizcards,
  // prettier-ignore
  getBizcardsBySelectedTopics: (state, getters, rootState, rootGetters) => (bizs) => {
    const filtered = bizs || getters.getBizcards;
    const topics = rootGetters['filters/getSelectedTopics'];

    if (topics.length) {
      const selectedTopics = new Set(topics);

      return filtered.filter((bizcard) =>
        bizcard.topics.some((topic) => selectedTopics.has(topic.code))
      );
    }

    return filtered;
  },
  // prettier-ignore
  getBizcardsBySelectedTypes: (state, getters, rootState, rootGetters) => (bizs) => {
    const filtered = bizs || getters.getBizcards;
    const types = rootGetters['filters/getSelectedTypes'];

    if (types.length) {
      const selectedTypes = new Set(types);

      return filtered.filter((bizcard) =>
        selectedTypes.has(bizcard.type.code)
      );
    }

    return filtered;
  },
  // prettier-ignore
  getBizcardsBySelectedGeofocus: (state, getters, rootState, rootGetters) => (bizs) => {
    const filtered = bizs || getters.getBizcards;
    const geofocus = rootGetters['filters/getSelectedGeofocus'];

    if (geofocus.length) {
      const selectedGeofocus = new Set(geofocus);

      return filtered.filter((bizcard) =>
        selectedGeofocus.has(bizcard.geofocus.code)
      );
    }

    return filtered;
  },
  getFilteredBizcards: (state, getters) => {
    const {
      getBizcards: all,
      getBizcardsBySelectedTopics: filterBySelectedTopics,
      getBizcardsBySelectedTypes: filterBySelectedTypes,
      getBizcardsBySelectedGeofocus: filterBySelectedGeofocus,
    } = getters;

    return filterBySelectedGeofocus(
      filterBySelectedTypes(filterBySelectedTopics(all))
    );
  },
  getFilteredBizcardsCount: (state, getters) => {
    return getters.getFilteredBizcards.length;
  },
  getSelectedBizcard: (state) => state.selected,
};
