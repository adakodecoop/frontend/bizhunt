// prettier-ignore
export default ({ store, route, redirect }) => {
  // console.log('[middleware/navigate-if-allowed]');

  const currentRoute = route.fullPath;
  if (/(server-error|no-active-bizcards)/.test(currentRoute)) {
    return;
  }

  const fromRoute = store.getters['routing/getFromRoute'];
  if (/(server-error(.+bizcards?.fetch.error)?|no-active-bizcards)$/.test(fromRoute)) {
    return redirect(fromRoute);
  }
};
