const users = require('../data/users.js');

exports.seed = async (knex) => {
  // Delete ALL existing entries
  await knex('users-permissions_user').truncate();
  // Insert seed entries
  await knex('users-permissions_user').insert(users);
};
