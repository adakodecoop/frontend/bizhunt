import dayjs from 'dayjs';

/**
 *
 * @param {Number} amount
 */
export const asCurrency = (amount, currency = 'EUR') => {
  const options = { style: 'currency', currency };
  return new Intl.NumberFormat('es-ES', options).format(amount);
};

/**
 *
 * @param {Date} date
 */
export const asDateObject = (date, options = {}) => {
  const defaults = {
    day: 'DD',
    month: 'MM',
    year: 'YYYY',
    hour: 'HH',
    minute: 'mm',
  };
  const { day, month, year, hour, minute } = { ...defaults, options };

  return {
    isoDate: date,
    formattedDate: dayjs(date).format(`${day}/${month}/${year}`),
    formattedHour: dayjs(date).format(`${hour}:${minute}`),
  };
};
