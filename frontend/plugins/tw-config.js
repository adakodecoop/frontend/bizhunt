import { theme } from '~tailwind.config';

export default (ctx, inject) => {
  inject('theme', theme);
};
