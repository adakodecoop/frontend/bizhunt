import { Meta, Preview, Props, Story } from '@storybook/addon-docs/blocks';
import { DependenciesTable } from 'storybook-addon-deps/blocks';
import { withKnobs, object } from '@storybook/addon-knobs';

import AStackL from './AStackL.vue';

<Meta
  title="Core/Layouts/AStackL"
  component={AStackL}
  parameters={{ component: AStackL }}
/>

# A-Stack-L

Injects margin between elements via their common parent.

<Preview>
  <Story name="default" decorators={[withKnobs]}>
    {{
      components: { AStackL },
      props: {
        styles: {
          default: () => object(
            'CSS Variables',
            {
              '--a-stack-l--space-y': '1.5rem',
            },
            'Styles'
          ),
        },
      },
      template: `
        <a-stack-l :style="styles">
          <span class="p-2 bg-gray-100">Stacked element 1</span>
          <span class="p-2 bg-gray-100">Stacked element 2</span>
          <span class="p-2 bg-gray-100">Stacked element 3</span>
        </a-stack-l>
      `,
    }}
  </Story>
</Preview>

## API

<Props of={AStackL} />

## Installation

```bash
yarn add -E @bit/adakode.ak-ui.core.layouts.a-stack
```

## Usage

> Anywhere elements are stacked one atop another, it is likely a Stack should be in effect.

```html
<!-- Basic Stack -->
<a-stack-l>
  <h2><!-- some text --></h2>
  <img src="path/to/some/image.svg" />
  <p><!-- more text --></p>
</a-stack-l>
```

```html
<!-- Nested Stack -->
<a-stack-l style="--a-stack-l--space-y: 3rem">
  <h2><!-- heading label --></h2>
  <a-stack-l>
    <p><!-- body text --></p>
    <p><!-- body text --></p>
    <p><!-- body text --></p>
  </a-stack-l>
  <h2><!-- heading label --></h2>
  <a-stack-l>
    <p><!-- body text --></p>
    <p><!-- body text --></p>
    <p><!-- body text --></p>
  </a-stack-l>
</a-stack-l>
```

```html
<!-- List Stack -->
<a-stack-l role="list">
  <div role="listitem"><!-- item 1 content --></div>
  <div role="listitem"><!-- item 2 content --></div>
  <div role="listitem"><!-- item 3 content --></div>
<a-stack-l>
```

```html
<!-- Recursive Stack -->
<a-stack-l class="a-stack-l--recursive">
  <div><!-- first level child --></div>
  <div><!-- first level sibling --></div>
  <div>
    <div><!-- second level child --></div>
    <div><!-- second level sibling --></div>
  </div>
<a-stack-l>
```

```html
<!-- Splitted Stack -->
<a-box-l>
  <a-stack-l class="a-stack-l--splitAfter-1">
    <div><!-- stacked element 1 at the top --></div>
    <div><!-- stacked element 2 at the bottom --></div>
  <a-stack-l>
</a-box-l>
```

## Customization

There are several ways to customize the look and feel of the component,
depending on the desired scope of the customization.

### Global Customization

To customize the look and feel of **every single instance** of the component
create a css file defining a class named after the kebab-case version of the component name
and define any of the custom properties below:

| CSS variable         |  default | design token       |
| :------------------- | -------: | :----------------- |
| `--a-stack-l--space-y` | `1.5rem` | `theme('space.6')` |

These are the steps:

```bash
mkdir -p src/assets/styles/components/ui/core/layouts && touch $_/AStackL.css
```

```css
/* <rootDir>/src/assets/styles/components/index.css */
@import './ui/core/layouts/AStackL.css';
```

```css
/* <rootDir>/src/assets/styles/components/ui/core/layouts/AStackL.css */
.a-stack-l {
  --a-stack-l--space-y: theme('space.6');
}
```

### Scoped Customization

To customize the look and feel of a **group of instances** of the component
augment the specificity of the component class with a scoping class:

```css
/* <rootDir>/src/assets/styles/components/ui/core/layouts/AStackL.css */
.a-stack-l.a-stack-l--outer {
  --a-stack-l--space-y: theme('space.8');
}
.a-stack-l.a-stack-l--inner {
  --a-stack-l--space-y: theme('space.4');
}
```

```html
<!-- <rootDir>/src/components/path/to/the/ConsumerComponent.vue -->
<a-stack-l class="a-stack-l--outer">
  <div><!-- first level child --></div>
  <div><!-- first level sibling --></div>
  <a-stack-l class="a-stack-l--inner">
    <div><!-- second level child --></div>
    <div><!-- second level sibling --></div>
  </a-stack-l>
<a-stack-l>
```

### Instance Customization

To customize the look and feel of a **single instance** of the component
you can define the desired CSS variables inline:

```html
<!-- Using CSS variables -->
<a-stack-l
  :style="{
    '--a-stack-l--space-y': '0.5rem',
  }"
>
  <div><!-- stacked element 1 --></div>
  <div><!-- stacked element 2 --></div>
  <div><!-- stacked element 3 --></div>
</a-stack-l>
```

or use the proper utility classes after a `reset` class:

```html
<!-- Using utility classes -->
<a-stack-l class="reset space-y-2">
  <div><!-- stacked element 1 --></div>
  <div><!-- stacked element 2 --></div>
  <div><!-- stacked element 3 --></div>
<a-stack-l>
```

## Dependencies

<DependenciesTable
  titleDependencies="Dependencies"
  titleDependents="Dependents"
  of={AStackL}
/>

## Links

Source: https://gitlab.com/adakodecoop/coop/adakode-ui/-/tree/master/src/components/ui/core/layouts/AStack  
Bit: https://bit.dev/adakode/ak-ui/core/layouts/a-stack  
Every Layout: https://absolutely.every-layout.dev/layouts/stack/
