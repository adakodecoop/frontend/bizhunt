const { Machine, assign, sendParent } = require('xstate');

/**
 *
 */
const events = {
  DELIVERY_START: 'DELIVERY.START',
  DELIVERY_DONE: 'DELIVERY.DONE',
};

/**
 *
 */
const machine = Machine(
  {
    id: 'email-delivery',
    initial: 'idle',
    context: {
      message: {},
      outcome: {},
      intervals: [],
    },
    states: {
      idle: {
        on: {
          [events.DELIVERY_START]: 'pending',
        },
      },
      pending: {
        invoke: {
          id: 'send-message',
          src: 'sendMessage',
          onDone: {
            target: 'done',
            actions: ['logSuccess'],
          },
          onError: [
            { target: 'waiting', cond: 'retry' },
            { target: 'done', actions: ['logError'] },
          ],
        },
      },
      waiting: {
        after: {
          DELIVERY_DELAY: 'pending',
        },
        exit: ['updateIntervals'],
      },
      done: {
        entry: ['notifyOutcome'],
        type: 'final',
      },
    },
  },
  {
    actions: {
      updateIntervals: assign({
        intervals: ({ intervals }) => intervals.slice(1),
      }),
      logSuccess: assign({
        outcome: {
          status: 'fulfilled',
        },
      }),
      logError: assign({
        outcome: ({ message }) => ({
          status: 'rejected',
          recipient: message.to,
        }),
      }),
      notifyOutcome: sendParent(({ outcome }) => ({
        type: events.DELIVERY_DONE,
        outcome,
      })),
    },
    delays: {
      DELIVERY_DELAY: ({ intervals }) => intervals[0] * 60 * 1000,
    },
    guards: {
      retry: ({ intervals }) => intervals.length > 0,
    },
    services: {
      sendMessage: ({ message }) => {
        return strapi.plugins.email.services.email.send(message);
      },
    },
  }
);

module.exports = {
  emailDeliveryMachine: machine,
  emailDeliveryEvents: events,
};
