import { Meta, Preview, Props, Story } from '@storybook/addon-docs/blocks';
import { DependenciesTable } from 'storybook-addon-deps/blocks';
import { withKnobs, object } from '@storybook/addon-knobs';

import ACoverL from './ACoverL.vue';

<Meta
  title="Core/Layouts/ACoverL"
  component={ACoverL}
  parameters={{ component: ACoverL }}
/>

# A-Cover-L

The Cover has one principal element that should always gravitate towards the center.
In addition, it can have one top/header element and/or one bottom/footer element.

<Preview>
  <Story name="default" decorators={[withKnobs]}>
    {{
      components: { ACoverL },
      props: {
        styles: {
          default: () =>
            object(
              'CSS Variables',
              {
                '--a-cover-l--min-h': '25vh',
                '--a-cover-l--px': '0',
                '--a-cover-l--py': '0',
                '--a-cover-l--child--my': '0',
              },
              'Styles'
            ),
        },
      },
      template: `
        <a-cover-l class="bg-gray-200" :style="styles">
          <a-box-l class="reset bg-gray-50" data-centered>
            <h2>This is a vertically centered box</h2>
          </a-box-l>
        </a-cover-l>
      `,
    }}
  </Story>
</Preview>

## API

<Props of={ACoverL} />

## Installation

```bash
yarn add -E @bit/adakode.ak-ui.core.layouts.a-cover
```

## Usage

> A typical use would be to create the “above the fold” introductory content for a web page.

```html
<!-- Basic Cover -->
<a-cover-l>
  <header><!-- header content --></header>
  <h1 data-centered><!-- main heading --></h1>
  <footer><!-- footer content --></footer>
</a-cover-l>
```

## Customization

There are several ways to customize the look and feel of the component,
depending on the desired scope of the customization.

### Global Customization

To customize the look and feel of **every single instance** of the component
create a css file defining a class named after the kebab-case version of the component name
and define any of the custom properties below:

| CSS variable             | default | design token                |
| :----------------------- | ------: | :-------------------------- |
| `--a-cover-l--min-h`     | `100vh` | `theme('minHeight.screen')` |
| `--a-cover-l--px`        |     `0` | `theme('padding.0')`        |
| `--a-cover-l--py`        |     `0` | `theme('padding.0')`        |
| `--a-cover-l--child--my` |     `0` | `theme('margin.0')`         |

These are the steps:

```bash
mkdir -p src/assets/styles/components/ui/core/layouts && touch $_/ACoverL.css
```

```css
/* <rootDir>/src/assets/styles/components/index.css */
@import './ui/core/layouts/ACoverL.css';
```

```css
/* <rootDir>/src/assets/styles/components/ui/core/layouts/ACoverL.css */
.a-cover-l {
  --a-cover-l--min-h: theme('minHeight.screen');
  --a-cover-l--px: 0;
  --a-cover-l--py: 0;
  --a-cover-l--child--my: 0;
}
```

### Scoped Customization

To customize the look and feel of a **group of instances** of the component
augment the specificity of the component class with a scoping data attribute:

```css
/* <rootDir>/src/assets/styles/components/ui/core/layouts/ACoverL.css */
.a-cover-l[data-cover-size='small'] {
  --a-cover-l--min-h: 25vh;
}
.a-cover-l[data-cover-size='medium'] {
  --a-cover-l--min-h: 50vh;
}
.a-cover-l[data-cover-size='large'] {
  --a-cover-l--min-h: 100vh;
}
```

```html
<!-- <rootDir>/src/components/path/to/the/ConsumerComponent.vue -->
<a-cover-l data-cover-size='medium'>
  <header><!-- header content --></header>
  <h1 data-centered><!-- main heading --></h1>
  <footer><!-- footer content --></footer>
</a-cover-l>
```

### Instance Customization

To customize the look and feel of a **single instance** of the component
you can define the desired CSS variables inline:

```html
<!-- Using CSS variables -->
<a-cover-l
  :style="{
    '--a-cover-l--min-h': '100vh',
    '--a-cover-l--px': '0',
    '--a-cover-l--py': '0',
    '--a-cover-l--child--my': '0',
  }"
/>
  <header><!-- header content --></header>
  <h1 data-centered><!-- main heading --></h1>
  <footer><!-- footer content --></footer>
</a-cover-l>
```

or use the proper utility classes after a `reset` class:

```html
<!-- Using utility classes -->
<a-cover-l class="reset min-h-screen px-0 py-0">
  <header><!-- header content --></header>
  <h1 data-centered><!-- main heading --></h1>
  <footer><!-- footer content --></footer>
</a-cover-l>
```

## Dependencies

<DependenciesTable
  titleDependencies="Dependencies"
  titleDependents="Dependents"
  of={ACoverL}
/>

## Links

Source: https://gitlab.com/adakodecoop/coop/adakode-ui/-/tree/master/src/components/ui/core/layouts/ACover  
Bit: https://bit.dev/adakode/ak-ui/core/layouts/a-cover  
Every Layout: https://absolutely.every-layout.dev/layouts/cover/
