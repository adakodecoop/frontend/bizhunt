'use strict';

const outdent = require('outdent');
const mjml = require('mjml');
const dayjs = require('dayjs');

const editedAt = () => dayjs(new Date()).format('DD/MM/YYYY');

module.exports = {
  text: ({ issue, ubizs }) => {
    const bizs = ubizs.reduce((list, biz) => {
      const bizref = `${biz.title} · ${biz.source} · ${process.env.BH_FRONTEND_URL}/${biz.slug}`;

      return `
        ${list}
        ${bizref}
      `;
    }, '');

    return outdent`
      Biz Hunt by Citilab · Issue #${issue}
      Editat el ${editedAt()}

      Aquestes són les convocatòries publicades durant la darrera setmana:

      -.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-

      ${bizs}
    `;
  },
  html: ({ issue, ubizs }) => {
    const bizhuntLogoUrl = 'https://bizhunt-cms.citilab.eu/uploads/logo_bizhunt_281a057a07.png';

    const head = `
      <mj-title>Biz Hunt by Citilab · Issue #${issue}</mj-title>
      <mj-font name="Roboto" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500"></mj-font>
      <mj-attributes>
        <mj-all font-family="Roboto, sans-serif"></mj-all>
        <mj-text font-weight="300" font-size="16px" color="#616161" line-height="24px"></mj-text>
        <mj-section padding="0"></mj-section>
      </mj-attributes>
    `;

    const header = `
      <mj-section padding="20px">
        <mj-column width="50%">
          <mj-image src="${bizhuntLogoUrl}"></mj-image>
        </mj-column>
      </mj-section>

      <mj-section>
        <mj-column width="100%">
          <mj-text>
            <h1 style="margin: 0; font-weight: 500; font-size: 24px">Biz Hunt by Citilab · Issue #${issue}</h1>
            <p style="margin: 10px 0">Editat el ${editedAt()}</p>
            <p style="margin: 30px 0 0">Aquestes són les convocatòries publicades durant la darrera setmana</p>
          </mj-text>
        </mj-column>
      </mj-section>

      <mj-section>
        <mj-column width="100%">
          <mj-divider border-width="2px" border-color="#9FA6B2" padding-bottom="30px"></mj-divider>
        </mj-column>
      </mj-section>
    `;

    const content = ubizs.map((biz) => {
      const [day, month, year] = dayjs(biz.date)
        .format('DD/MM/YYYY')
        .split('/');

      const topics = biz.topics.reduce((markup, topic) => {
        return `
            ${markup}
            <span style="padding: 4px 8px; border-radius: 4px; font-size: 16px; color: #252F3F; background-color: ${topic.color};">${topic.name}</span>
          `;
      }, '');

      return `
        <mj-section background-color="${biz.type.color}" border-radius="4px">
          <mj-column width="10%">
            <mj-text padding="6px 8px" font-size="14px" line-height="0.5" color="#F9FAFB">
              <p>${day}/${month}</p>
              <p>${year}</p>
            </mj-text>
          </mj-column>
          <mj-column width="90%" background-color="#F9FAFB">
            <mj-text>
              <h2 style="margin:0; font-weight: 400; font-size: 18px;">
                <a href="${process.env.BH_FRONTEND_URL}/${biz.slug}">${biz.title}</a>
              </h2>
              <p style="margin: 5px 0; font-size: 14px; color: #4B5563">
                ${biz.source}
              </p>
              <p style="margin: 25px 0 5px">
                ${topics}
              </p>
            </mj-text>
          </mj-column>
        </mj-section>
      `;
    }).join(`
        <mj-section>
          <mj-column width="100%">
            <mj-divider border-width="2px" border-color="#E5E7EB"></mj-divider>
          </mj-column>
        </mj-section>
    `);

    const footer = `
      <mj-section>
        <mj-column width="100%">
          <mj-divider border-width="2px" border-color="#9FA6B2" padding-top="30px"></mj-divider>
        </mj-column>
      </mj-section>

      <mj-section>
        <mj-column width="30%" >
          <mj-image src="https://www.citilab.eu/wp-content/uploads/2017/02/LogoCitilab3D-768x768.png"></mj-image>
        </mj-column>
        <mj-column width="70%">
          <mj-text padding-top="12px" color="#4B5563" font-size="12px" align="right">
            <p>FUNDACIÓ PEL FOMENT DE LA SOCIETAT DEL CONEIXEMENT (CITILAB)</p>
            <a href="https://www.citilab.eu" style="color: #161E2E; text-decoration: none">www.citilab.eu</a>
          </mj-text>
        </mj-column>
      </mj-section>
    `;

    const template = `
      <mjml>
        <mj-head>
          ${head}
        </mj-head>
        <mj-body background-color="#E5E7EB">
          ${header}
          <mj-wrapper padding="0 25px">
          ${content}
          </mj-wrapper>
          ${footer}
        </mj-body>
      </mjml>
    `;

    return mjml(template, { minify: true }).html;
  },
};
