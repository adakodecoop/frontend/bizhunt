// prettier-ignore
const { bulkEmailDeliveryMachine, bulkEmailDeliveryEvents } = require('./email/fsms/bulkEmailDelivery');
const { createService } = require('./email/fsms/core');
const templateFactory = require('./email/templates');

// prettier-ignore
/**
 *
 */
module.exports = async (freq = 'w') => {
  strapi.log.info('[sendDigest] START');

  const { sentAt: lastSentAt, issue: lastIssue } = await findLastDigest(freq);
  const bizs = await findPublishedBizcardsSince(lastSentAt);

  if (bizs.length) {
    const digest = { issue: lastIssue + 1, freq };
    const users = await findActiveUsers();
    const udigests = buildUsersDigests(digest, users, bizs);
    const { delivered, failed } = await sendUsersDigests(udigests);
    await logDigest(digest, delivered, failed);
  }

  strapi.log.info('[sendDigest] END');
};

/**
 *
 */
const findLastDigest = async (freq) => {
  // NOTE: make sure there's a zero digest entry with freq
  const digests = await strapi.services.digest.find({
    freq,
    _sort: 'sentAt:DESC',
    _limit: 1,
  });

  return digests[0];
};

/**
 *
 */
const findPublishedBizcardsSince = async (date) => {
  return await strapi.services.bizcard.find(
    {
      updated_at_gte: date,
      _sort: 'updated_at:DESC',
    },
    ['topics', 'type']
  );
};

/**
 *
 */
const findActiveUsers = async () => {
  // NOTE: make sure there's a zero active user that receives all sent digests
  // with profile.subscribed=['d','w'] and profile.interests=['all','the','topics']
  // return = await strapi.plugins['users-permissions'].services.user.fetchAll({
  //   blocked: false,
  //   confirmed: true,
  // }, [ 'profile' ]);
  return await strapi.plugins['users-permissions'].services.user.fetchAll({
    blocked: false,
    confirmed: true,
  });
};

/**
 *
 */
const buildUsersDigests = ({ issue, freq }, users, bizs) => {
  const udigests = users
    // .filter((user) => user.profile.subscribed.includes(freq))
    .map((user) => {
      // const utopics = new Set(user.profile.interests);
      // const ubizs = bizs.filter((biz) => biz.topics.some((topic) => utopics.has(topic)));
      const ubizs = bizs;
      const digest = buildDigest(freq, { issue, user, ubizs });
      return digest;
    });

  return udigests;
};

/**
 *
 */
const buildDigest = (freq, data) => {
  const { text, html } = templateFactory.build(freq, data);

  return {
    to: data.user.email,
    from: process.env.BH_DIGEST_EMAIL_FROM,
    replyTo: process.env.BH_DIGEST_EMAIL_REPLYTO,
    subject: `Biz Hunt by Citilab · Issue #${data.issue}`,
    text,
    html,
  };
};

/**
 *
 */
const sendUsersDigests = async (udigests) => {
  const outcomes = await send(udigests);
  const stats = aggregateStats(outcomes);

  return stats;
};

/**
 *
 */
const send = (udigests) => {
  const initialContext = {
    messages: udigests,
    outcomes: [],
    intervals: [1, 5],
  };
  let context;

  return new Promise((resolve) => {
    const service = createService(bulkEmailDeliveryMachine, initialContext)
      .onTransition((state) => (context = state.context))
      .onDone(() => resolve(context.outcomes))
      .start();

    service.send(bulkEmailDeliveryEvents.DELIVERY_START);
  });
};

/**
 *
 */
const aggregateStats = (outcomes) => {
  const { delivered, failed } = outcomes.reduce(
    (stats, outcome) => {
      if (outcome.status === 'fulfilled') {
        stats.delivered++;
      }

      if (outcome.status === 'rejected') {
        stats.failed.push(outcome.recipient);
      }

      return stats;
    },
    { delivered: 0, failed: [] }
  );

  return { delivered, failed };
};

/**
 *
 */
const logDigest = async ({ issue, freq }, delivered, failed) => {
  await strapi.services.digest.create({
    sentAt: new Date(),
    issue,
    freq,
    delivered,
    failed,
  });
};
