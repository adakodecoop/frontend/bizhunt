'use strict';

const dayjs = require('dayjs');
const slugify = require('slugify');

const checkRequired = strapi.config.functions['validations'].checkRequired;
const checkElegible = strapi.config.functions['validations'].checkElegible;

/**
 *
 * @param {Object} data
 */
const getValidTypes = async (data) => {
  const { details } = data;

  /* eslint-disable-next-line */
  if (!Boolean(details?.length)) {
    return [];
  }

  const types = {
    'bizcard.bizevent': ['cfe'],
    'bizcard.bizmisc': ['al', 'cs'],
    'bizcard.bizcall': ['aj', 'li', 'oc', 'pr', 'su'],
  };

  const { __component } = details[0];
  const codes = types[__component];

  const bt = await strapi.query('type').find({ code_in: codes });
  const typeIds = bt.map(({id}) => id);

  return typeIds;
};

/**
 *
 * @param {Object} data
 */
const buildSlug = async ({ date, topics, title }) => {
  /* eslint-disable-next-line */
  if (!Boolean(date) || !Boolean(topics?.length)) {
    return null;
  }

  const t = await strapi.query('topic').find({ id_in: topics });

  const _date = dayjs(date).format('YYYYMMDD');
  const _topics = t.map(({ code }) => code).join('-');
  const _title = slugify(title, { lower: true });

  return `${_date}--${_topics}--${_title}`;
};

/**
 *
 * @param {Object} data
 */
const normalize = async (data) => {
  data.slug = await buildSlug(data);
  return data;
};

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/models.html#lifecycle-hooks)
 * to customize this model
 */
module.exports = {
  lifecycles: {
    beforeCreate: async (data) => {
      checkRequired(data, [
        { field: 'title', label: 'Títol' },
      ]);
      checkElegible(data, {
        field: 'type',
        values: await getValidTypes(data),
        message: 'El tipus seleccionat no es correpon amb el tipus de bizcard triat',
      });

      data = await normalize(data);
    },
    beforeUpdate: async (params, data) => {
      const { id } = params;
      const existing = await strapi.query('bizcard').findOne({ id }, ['topics']);

      const publishing = Object.keys(data).includes('published_at') && Boolean(data.published_at);
      const saving = !Object.keys(data).includes('published_at');

      if (publishing) {
        checkRequired(existing, [
          { field: 'details', label: 'Detalls' },
          { field: 'topics', label: 'Topics' },
          { field: 'type', label: 'Tipus' },
          { field: 'geofocus', label: 'Geofocus' },
        ]);
      }

      if (saving) {
        const published = existing && Boolean(existing.published_at);
        checkRequired(data, [
          { field: 'title', label: 'Títol' },
          { field: 'details', label: 'Detalls', if: published },
          { field: 'topics', label: 'Topics', if: published },
          { field: 'type', label: 'Tipus', if: published },
          { field: 'geofocus', label: 'Geofocus', if: published },
        ]);
        checkElegible(data, {
          field: 'type',
          values: await getValidTypes(data),
          message: 'El tipus seleccionat no es correpon amb el tipus de bizcard triat',
        });

        data = await normalize(data);
      }
    },
  },
};
