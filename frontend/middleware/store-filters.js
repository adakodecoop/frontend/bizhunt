export default ({ store, query }) => {
  // console.log('[middleware/store-filters] query:', JSON.stringify(query));
  const topics = (query.t && query.t.split(',')) || [];
  const types = (query.bt && query.bt.split(',')) || [];
  const geofocus = (query.g && query.g.split(',')) || [];

  store.dispatch('filters/setSelectedTopics', topics);
  store.dispatch('filters/setSelectedTypes', types);
  store.dispatch('filters/setSelectedGeofocus', geofocus);
};
