import 'isomorphic-unfetch';
import { createClient } from 'villus';
import Q from './queries';

const gqlClient = {
  init({ url, token }) {
    const context = () => ({
      fetchOptions: token
        ? { headers: { Authorization: `Bearer ${token()}` } }
        : {},
    });

    return Object.create(this.api({ url, context }));
  },

  api: (options) => ({
    client: createClient(options),

    async get({ resource, params: variables = {} }) {
      const { data } = await this.client.executeQuery({
        query: Q[resource],
        variables,
      });

      return data;
    },
  }),
};

export default gqlClient;
