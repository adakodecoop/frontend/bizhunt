export const state = () => ({
  fromRoute: null,
});

export const mutations = {
  SET_FROM_ROUTE: (state, route) => (state.fromRoute = route),
};

export const actions = {
  setFromRoute: ({ commit }, route) => commit('SET_FROM_ROUTE', route),
};

export const getters = {
  getFromRoute: (state) => state.fromRoute,
};
