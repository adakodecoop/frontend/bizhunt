const { interpret } = require('xstate');

/**
 *
 */
const buildMachine = (machine, ctx = {}) => {
  return machine.withContext({
    ...machine.context,
    ...ctx,
  });
};

/**
 *
 */
const createService = (machine, ctx = {}) => {
  const customMachine = buildMachine(machine, ctx);
  return interpret(customMachine);
};

module.exports = {
  buildMachine,
  createService,
};
