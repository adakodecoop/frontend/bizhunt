export default ({ app, route, redirect }) => {
  // console.log('[middleware/check-authenticated]');
  if (!app.$authService.getUser()) {
    let query = null;

    if (route.name === 'index-slug') {
      query = { from: route.fullPath };
    }

    return redirect('/login', query);
  }
};
