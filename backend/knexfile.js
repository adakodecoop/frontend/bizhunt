require('dotenv').config();

const options = {
  connection: {
    host: process.env.DATABASE_HOST,
    port: process.env.DATABASE_PORT,
    database: process.env.DATABASE_NAME,
    user: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
  },
  seeds: {
    directory: __dirname + '/db/seeds',
  },
  migrations: {
    directory: __dirname + '/db/migrations',
    tableName: 'knex_migrations',
  },
};

module.exports = {
  development: {
    ...options,
    client: 'pg',
  },

  staging: {
    ...options,
    client: 'pg',
    pool: {
      min: 2,
      max: 10,
    },
  },

  production: {
    ...options,
    client: 'pg',
    pool: {
      min: 2,
      max: 10,
    },
  },
};
