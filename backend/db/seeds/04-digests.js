const digests = require('../data/digests.js');

exports.seed = async (knex) => {
  // Delete ALL existing entries
  await knex('digests').truncate();
  // Insert seed entries
  await knex('digests').insert(digests);
};
