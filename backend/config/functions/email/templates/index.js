'use strict';

const weekly = require('./weeklyDigest');

const templates = {
  w: weekly,
};

module.exports = {
  build: (template, data) => ({
    text: templates[template].text(data),
    html: templates[template].html(data),
  }),
};
