export default `
  query getGeofocus {
    geofoci {
      id
      code
      name
      color
    }
  }
`;
