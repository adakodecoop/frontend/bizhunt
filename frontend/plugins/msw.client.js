import { setupWorker } from 'msw';
import mocks from '~/mocks';

export default async ({ isDev }) => {
  if (isDev && mocks.length) {
    const worker = setupWorker(...mocks);
    await worker.start();
  }
};
