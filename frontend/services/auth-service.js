const AuthService = (client) => ({
  async login({ identifier, password }) {
    await client.login({ identifier, password });
  },
  logout() {
    client.logout();
  },
  async forgotPassword({ email }) {
    await client.forgotPassword({ email });
  },
  async resetPassword({ code, password, passwordConfirmation }) {
    await client.resetPassword({
      code,
      password,
      passwordConfirmation,
    });
  },
  getUser() {
    return client.user;
  },
  getToken() {
    return client.getToken();
  },
});

export default AuthService;
