export default `
  query getTypes {
    types {
      id
      code
      name
      color
    }
  }
`;
