'use strict';

const { sanitizeEntity } = require('strapi-utils');
const { transformEntity } = strapi.config.functions['transforms'];

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/concepts/controllers.html#core-controllers)
 * to customize this controller
 */
module.exports = {
  /**
   * Retrieve records.
   *
   * @return {Array}
   */
  async find(ctx) {
    let entities;

    ctx.query = { ...ctx.query, date_gte: new Date(), _sort: 'updated_at:DESC' };

    if (ctx.query._q) {
      entities = await strapi.services.bizcard.search(ctx.query);
    } else {
      entities = await strapi.services.bizcard.find(ctx.query);
    }

    return entities.map((e) => sanitizeEntity(transformEntity(e, 'bizcard'), { model: strapi.models.bizcard }));
  }
};
