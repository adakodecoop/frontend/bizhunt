import { Meta, Preview, Props, Story } from '@storybook/addon-docs/blocks';
import { DependenciesTable } from 'storybook-addon-deps/blocks';
import { withKnobs, boolean, object } from '@storybook/addon-knobs';

import ACenterL from './ACenterL.vue';

<Meta
  title="Core/Layouts/ACenterL"
  component={ACenterL}
  parameters={{ component: ACenterL }}
/>

# A-Center-L

Creates a centered 'stripe' of content within any container,
capping its width to preserve a reasonable measure.

<Preview>
  <Story name="default" decorators={[withKnobs]}>
    {{
      components: { ACenterL },
      props: {
        intrinsic: { default: boolean('intrinsic', true, 'Props') },
        styles: {
          default: () => object(
            'CSS Variables',
            {
              '--a-center-l--max-w': '60ch',
              '--a-center-l--px': '0',
            },
            'Styles'
          ),
        },
      },
      template: `
        <a-box-l class="reset bg-gray-200">
          <a-center-l class="bg-gray-50" :intrinsic="intrinsic" :style="styles">
            <p>
              Ornare arcu odio ut sem nulla pharetra diam sit. Urna duis convallis convallis tellus.
              Vitae tempus quam pellentesque nec. Condimentum lacinia quis vel eros donec. Id leo in vitae turpis.
              Ultrices gravida dictum fusce ut placerat orci nulla pellentesque dignissim.
            </p>
            <button class="mt-4 px-4 py-3 text-gray-50 bg-gray-600" type="button">Button</button>
            <p class="mt-4">
              Nulla facilisi morbi tempus iaculis urna id volutpat lacus laoreet. Montes nascetur ridiculus mus mauris.
              Quis imperdiet massa tincidunt nunc pulvinar sapien et. Et magnis dis parturient montes.
              Blandit aliquam etiam erat velit scelerisque in dictum non consectetur.
            </p>
          </a-center-l>
        </a-box-l>
      `,
    }}
  </Story>
</Preview>

## API

<Props of={ACenterL} />

## Installation

```bash
yarn add -E @bit/adakode.ak-ui.core.layouts.a-center
```

## Usage

> Whenever you wish something to be horizontally centered, the Center is your friend.

```html
<!-- Single column web page -->
<a-box-l>
  <a-center-l>
    <a-stack-l>
      <!-- Any flow content here (headings, paragraphs, etc) -->
    </a-stack-l>
  </a-center-l>
</a-box-l>
```

```html
<!-- Single column web page with strictly centered content-->
<a-box-l>
  <a-center-l class="text-align" intrinsic>
    <a-stack-l>
      <!-- Any flow content here (headings, paragraphs, etc) -->
    </a-stack-l>
  </a-center-l>
</a-box-l>
```

## Customization

There are several ways to customize the look and feel of the component,
depending on the desired scope of the customization.

### Global Customization

To customize the look and feel of **every single instance** of the component
create a css file defining a class named after the kebab-case version of the component name
and define any of the custom properties below:

| CSS variable          | default | design token                |
| :-------------------- | ------: | :-------------------------- |
| `--a-center-l--max-w` |  `60ch` | `theme('maxWidth.measure')` |
| `--a-center-l--px`    |     `0` | `theme('spacing.0')`        |

These are the steps:

```bash
mkdir -p src/assets/styles/components/ui/core/layouts && touch $_/ACenterL.css
```

```css
/* <rootDir>/src/assets/styles/components/index.css */
@import './ui/core/layouts/ACenterL.css';
```

```css
/* <rootDir>/src/assets/styles/components/ui/core/layouts/ACenterL.css */
.a-center-l {
  --a-center-l--max-w: 60ch;
  --a-center-l--px: 0;
}
```

### Scoped Customization

To customize the look and feel of a **group of instances** of the component
augment the specificity of the component class with a scoping class:

```css
/* <rootDir>/src/assets/styles/components/ui/core/layouts/ACenterL.css */
.a-center-l.a-center-l--outer {
  --a-center-l--px: theme('spacing.2');
}
.a-center-l.a-center-l--inner {
  --a-center-l--px: theme('spacing.4');
}
```

```html
<!-- <rootDir>/src/components/path/to/the/ConsumerComponent.vue -->
<a-center-l class="a-center-l--outer">
  <a-stack-l>
    <p><!-- A paragraph --></p>
    <a-center-l class="a-center-l--inner">
      <p><!-- A paragraph --></p>
    </a-center-l>
    <p><!-- A paragraph --></p>
  </a-stack-l>
</a-center-l>
```

### Instance Customization

To customize the look and feel of a **single instance** of the component
you can define the desired CSS variables inline:

```html
<!-- Using CSS variables -->
<a-center-l
  :style="{
    '--a-center-l--max-w': '60ch',
    '--a-center-l--px': '0',
  }"
>
  <!-- Any flow content here (headings, paragraphs, etc) -->
</a-center-l>
```

or use the proper utility classes after a `reset` class:

```html
<!-- Using utility classes -->
<a-center-l class="reset max-w-measure px-0">
  <!-- Any flow content here (headings, paragraphs, etc) -->
</a-center-l>
```

## Dependencies

<DependenciesTable
  titleDependencies="Dependencies"
  titleDependents="Dependents"
  of={ACenterL}
/>

## Links

Source: https://gitlab.com/adakodecoop/coop/adakode-ui/-/tree/master/src/components/ui/core/layouts/ACenter  
Bit: https://bit.dev/adakode/ak-ui/core/layouts/a-center  
Every Layout: https://absolutely.every-layout.dev/layouts/center/
