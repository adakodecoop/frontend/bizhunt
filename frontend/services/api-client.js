import gqlClient from './graqhql/client';

const ApiClient = {
  client: {
    graphql: gqlClient,
  },

  init({ client, type, options }) {
    if (client) return client;
    return this.client[type].init(options);
  },
};

export default ApiClient;
