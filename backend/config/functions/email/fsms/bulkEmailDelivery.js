const {
  Machine,
  assign,
  actions: { pure },
  send,
  spawn,
} = require('xstate');

const { emailDeliveryMachine } = require('./emailDelivery');

/**
 *
 */
const events = {
  DELIVERY_START: 'DELIVERY.START',
  DELIVERY_DONE: 'DELIVERY.DONE',
};

/**
 *
 */
const machine = Machine(
  {
    id: 'bulk-email-delivery',
    initial: 'idle',
    context: {
      messages: [],
      outcomes: [],
      intervals: [],
      actors: [],
    },
    states: {
      idle: {
        entry: ['createActors'],
        on: {
          [events.DELIVERY_START]: {
            target: 'waiting',
            actions: ['notifyActors'],
          },
        },
      },
      waiting: {
        on: {
          [events.DELIVERY_DONE]: {
            target: 'checking',
            actions: ['updateOutcomes'],
          },
        },
      },
      checking: {
        always: [
          { target: 'complete', cond: 'noPendingActors' },
          { target: 'waiting' },
        ],
      },
      complete: {
        type: 'final',
      },
    },
  },
  {
    actions: {
      createActors: assign({
        actors: ({ messages, intervals }) => {
          return messages.map((message) => {
            const ctx = { ...emailDeliveryMachine.context, message, intervals };
            return spawn(emailDeliveryMachine.withContext(ctx));
          });
        },
      }),
      notifyActors: pure(({ actors }) => {
        return actors.map((actor, index) => {
          return send(events.DELIVERY_START, {
            delay: index * 250,
            to: actor,
          });
        });
      }),
      updateOutcomes: assign({
        outcomes: ({ outcomes }, { outcome }) => [...outcomes, outcome],
      }),
    },
    guards: {
      noPendingActors: ({ messages, outcomes }) => {
        return messages.length === outcomes.length;
      },
    },
  }
);

module.exports = {
  bulkEmailDeliveryMachine: machine,
  bulkEmailDeliveryEvents: events,
};
