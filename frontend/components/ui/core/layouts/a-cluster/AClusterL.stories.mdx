import { Meta, Preview, Props, Story } from '@storybook/addon-docs/blocks';
import { DependenciesTable } from 'storybook-addon-deps/blocks';
import { withKnobs, object } from '@storybook/addon-knobs';

import AClusterL from './AClusterL.vue';
import ABadgeUi from '../../atoms/ABadge/ABadgeUi.vue';

<Meta
  title="Core/Layouts/AClusterL"
  component={AClusterL}
  parameters={{ component: AClusterL }}
/>

# A-Cluster-L

Deals with groups of elements of an indeterminate size/shape
that should distribute in a similarly fluid way.

<Preview>
  <Story name="default" decorators={[withKnobs]}>
    {{
      components: { AClusterL, ABadgeUi },
      props: {
        styles: {
          default: () =>
            object(
              'CSS Variables',
              {
                '--a-cluster-l--justify': 'flex-start',
                '--a-cluster-l--align': 'center',
                '--a-cluster-l--space': '0.5rem',
                '--a-cluster-l--overflow-x': 'hidden',
                '--a-cluster-l--overflow-y': 'hidden',
              },
              'Styles'
            ),
        },
      },
      template: `
        <a-box-l class="reset w-48 p-1 bg-gray-200">
          <a-cluster-l :style="styles">
            <div>
              <a-badge-ui>Foo</a-badge-ui>
              <a-badge-ui>Foo-Bar</a-badge-ui>
              <a-badge-ui>Bar</a-badge-ui>
              <a-badge-ui>Bar-Baz</a-badge-ui>
              <a-badge-ui>Baz</a-badge-ui>
            </div>
          </a-cluster-l>
        </a-box-l>
      `,
    }}
  </Story>
</Preview>

## API

<Props of={AClusterL} />

## Installation

```bash
yarn add -E @bit/adakode.ak-ui.core.layouts.a-cluster
```

## Usage

> Cluster components suit any groups of elements that differ in length and are
> liable to wrap. Buttons that appear together at the end of forms are ideal
> candidates, as well as lists of tags, keywords, or other meta information. Use
> the Cluster to align any groups of horizontally laid out elements to the left or
> right, or in the center.

```html
<!-- Basic Cluster -->
<a-cluster-l>
  <!-- intermediary wrapper -->
  <div>
    <!-- child element here -->
    <!-- another child element -->
    <!-- etc -->
    <!-- etc -->
    <!-- etc -->
  </div>
</a-cluster-l>
```

```html
<!-- List Cluster -->
<a-cluster-l>
  <!-- intermediary wrapper -->
  <ul>
    <li><!-- content of first list item --></li>
    <li><!-- content of second list item --></li>
    <li><!-- etc --></li>
    <li><!-- etc --></li>
    <li><!-- etc --></li>
  </ul>
</a-cluster-l>
```

## Customization

There are several ways to customize the look and feel of the component,
depending on the desired scope of the customization.

### Global Customization

To customize the look and feel of **every single instance** of the component
create a css file defining a class named after the kebab-case version of the component name
and define any of the custom properties below:

| CSS variable                |      default | design token         |
| :-------------------------- | -----------: | :------------------- |
| `--a-cluster-l--justify`    | `flex-start` | `n/a`                |
| `--a-cluster-l--align`      |     `center` | `n/a`                |
| `--a-cluster-l--space`      |     `0.5rem` | `theme('spacing.2')` |
| `--a-cluster-l--overflow-x` |     `hidden` | `n/a`                |
| `--a-cluster-l--overflow-y` |     `hidden` | `n/a`                |

These are the steps:

```bash
mkdir -p src/assets/styles/components/ui/core/layouts && touch $_/AClusterL.css
```

```css
/* <rootDir>/src/assets/styles/components/index.css */
@import './ui/core/layouts/AClusterL.css';
```

```css
/* <rootDir>/src/assets/styles/components/ui/core/layouts/AClusterL.css */
.a-cluster-l {
  --a-cluster-l--justify: flex-start;
  --a-cluster-l--align: center;
  --a-cluster-l--space: theme('spacing.2');
  --a-cluster-l--overflow-x: hidden;
  --a-cluster-l--overflow-y: hidden;
}
```

### Scoped Customization

To customize the look and feel of a **group of instances** of the component
augment the specificity of the component class with a scoping class:

```css
/* <rootDir>/src/assets/styles/components/ui/core/layouts/AClusterL.css */
.a-cluster-l.a-cluster-l--rtl {
  --a-cluster-l--justify: flex-end;
}
```

```html
<!-- <rootDir>/src/components/path/to/the/ConsumerComponent.vue -->
<!-- A right to left Cluster -->
<a-cluster-l class="a-cluster-l--rtl">
  <!-- intermediary wrapper -->
  <div>
    <!-- child element here -->
    <!-- another child element -->
    <!-- etc -->
    <!-- etc -->
    <!-- etc -->
  </div>
</a-cluster-l>
```

### Instance Customization

To customize the look and feel of a **single instance** of the component
you can define the desired CSS variables inline:

```html
<!-- Using CSS variables -->
<a-cluster-l
  :style="{
    '--a-cluster-l--justify': 'flex-start',
    '--a-cluster-l--align': 'center',
    '--a-cluster-l--space': '0.5rem',
    '--a-cluster-l--overflow-x': 'hidden',
    '--a-cluster-l--overflow-y': 'hidden',
  }"
>
  <!-- intermediary wrapper -->
  <div>
    <!-- child element here -->
    <!-- another child element -->
    <!-- etc -->
    <!-- etc -->
    <!-- etc -->
  </div>
</a-cluster-l>
```

or use the proper utility classes after a `reset` class:

```html
<!-- Using utility classes -->
<a-cluster-l class="reset overflow-x-hidden overflow-y-hidden">
  <!-- intermediary wrapper -->
  <div>
    <!-- child element here -->
    <!-- another child element -->
    <!-- etc -->
    <!-- etc -->
    <!-- etc -->
  </div>
</a-cluster-l>
```

## Dependencies

<DependenciesTable
  titleDependencies="Dependencies"
  titleDependents="Dependents"
  of={AClusterL}
/>

## Links

Source: https://gitlab.com/adakodecoop/coop/adakode-ui/-/tree/master/src/components/ui/core/layouts/ACluster  
Bit: https://bit.dev/adakode/ak-ui/core/layouts/a-cluster  
Every Layout: https://absolutely.every-layout.dev/layouts/cluster/
