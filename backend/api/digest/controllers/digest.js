'use strict';

/* eslint-disable no-unused-vars */

/**
 * Read the documentation (https://strapi.io/documentation/v3.x/concepts/controllers.html#core-controllers)
 * to customize this controller
 */
module.exports = {
  /**
   *
   * @param {Object} ctx
   */
  send(ctx) {
    try {
      strapi.config.functions.sendDigest();

      return {
        statusCode: 202,
        body: JSON.stringify({ message: 'Accepted' }),
      };
    } catch (e) {
      strapi.log.error(`[sendDigest] ERROR: ${e.message}`);

      return {
        statusCode: 500,
        body: JSON.stringify({ message: e.message }),
      };
    }
  }
};
