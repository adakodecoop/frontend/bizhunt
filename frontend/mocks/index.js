import bizcardsMocks from './bizcards';
import taxonomiesMocks from './taxonomies';

export default [...bizcardsMocks, ...taxonomiesMocks];
