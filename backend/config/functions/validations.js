'use strict';

module.exports = {
  /**
   *
   * @param {Object} data
   * @param {Array} fields
   */
  checkRequired(data, fields) {
    const _fields = fields.reduce((F, { field, label, if: cond = true }) => {
      if (cond) {
        if (data[field] && Array.isArray(data[field]) && !data[field].length) F.push(label);
        if (!data[field]) F.push(label);
      }
      return F;
    }, []);

    if (_fields.length) {
      strapi.config.functions['custom-errors'].emitRequired(_fields);
    }
  },
  /**
   * @param {Object} data
   * @param {Object} dependencies
   */
  checkDependent(data, dependencies) {
    const { field, dependent, message, if: cond = true } = dependencies;

    if (cond) {
      const isFieldSet = Boolean(data[field]);
      const isDependentSet = Boolean(data[dependent]);

      if ((isFieldSet && !isDependentSet) || (!isFieldSet && isDependentSet)) {
        strapi.config.functions['custom-errors'].emitMessage(message);
      }
    }
  },

  /**
   * @param {Object} data
   * @param {Object} params
   */
  checkElegible(data, params) {
    const { field, values, message, if: cond = true } = params;

    if (cond) {
      const isFieldSet = Boolean(data[field]);

      if (isFieldSet && values.length && !values.includes(data[field])) {
        strapi.config.functions['custom-errors'].emitMessage(message);
      }
    }
  }
};
