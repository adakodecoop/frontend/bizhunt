# Infraestructura

## Descripció

La infraestructura utilitzada per posar en marxa l'aplicació consta de tres peces:

- *Repositori de codi*: El codi de l'aplicació està allotjat a [GitHub](https://github.com/).
- *Gestor de continguts*: Hem utilitzat [Strapi](https://strapi.io/), un gestor de continguts conegut per la seva flexibilitat i facilitat d'ús. [Més informació](https://bejamas.io/discovery/headless-cms/strapi/).
- *Hosting*: L'aplicació està allotjada a la infraestructura de [Hetzner](https://www.hetzner.com/), un dels proveïdors més importants d'Europa.

## Credencials

Aquestes són les credencials d'accés a la infraestructura utilitzada:

| Servei | Proveïdor | URL accés | Usuària | Contrasenya |
| :--- | :--- | :--- | :--- | :--- |
| Repositori de codi | GitHub | https://github.com/citilab-cornella | tic2@e-citilab.eu | j8VKQFte |
| Gestor de continguts | Strapi | https://bizhunt-cms.citilab.eu/admin/ | tic2@e-citilab.eu | j8VKQFte |
| Hosting | Hetzner | https://console.hetzner.cloud/projects | tic2@e-citilab.eu | j8VKQFte |

I aquestes les credencials d'accés als recursos utilitzats:

| Servei | Proveïdor | Accés | Usuària | Contrasenya |
| :--- | :--- | :--- | :--- | :--- |
| Servidor | Hetzner | https://console.hetzner.cloud/projects/658996/servers/9143096/overview | cleavr | WGMxVld2R29rVTBfSk83dVpMN0FUbHM2WFZsck13 |
| Base de Dades | PostgreSQL | n/a | cleavr | JUs-wMYgpoMctG0U3IcqHg |
