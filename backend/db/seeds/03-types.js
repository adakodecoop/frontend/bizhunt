const types = require('../data/types.json');

exports.seed = async (knex) => {
  // Delete ALL existing entries
  await knex('types').truncate();
  // Insert seed entries
  await knex('types').insert(types);
};
