import bizcards from './getBizcards';
import bizcard from './getBizcard';
import topics from './getTopics';
import types from './getTypes';
import geofocus from './getGeofocus';

const queries = {
  /* Bizcards */
  bizcards,
  bizcard,

  /* Taxonomies */
  topics,
  types,
  geofocus,
};

export default queries;
