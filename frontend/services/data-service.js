import { asCurrency, asDateObject } from '~/utils/transformers';

const DataService = (client) => ({
  /**
   *
   */
  async getTopics(params = {}) {
    const { topics } = await client.get({ resource: 'topics', params });

    if (topics) {
      return Object.freeze(topics);
    }

    throw new Error('ServerError');
  },
  /**
   *
   */
  async getTypes(params = {}) {
    const { types } = await client.get({ resource: 'types', params });

    if (types) {
      return Object.freeze(types);
    }

    throw new Error('ServerError');
  },
  /**
   *
   */
  async getGeofocus(params = {}) {
    // prettier-ignore
    const { geofoci: geofocus } = await client.get({ resource: 'geofocus', params });

    if (geofocus) {
      return Object.freeze(geofocus);
    }

    throw new Error('ServerError');
  },
  /**
   *
   */
  async getBizcards(params = {}) {
    const { bizcards } = await client.get({ resource: 'bizcards', params });

    if (!bizcards) {
      throw new Error('ServerError');
    }
    if (!bizcards.length) {
      throw new Error('NoActiveBizcards');
    }

    // prettier-ignore
    const bizs = bizcards.map((biz) => ({ ...biz, date: asDateObject(biz.date) }));
    return Object.freeze(bizs);
  },
  /**
   *
   */
  async getBizcard(params = {}) {
    const { bizcards } = await client.get({ resource: 'bizcard', params });

    if (!bizcards) {
      throw new Error('ServerError');
    }
    if (!bizcards.length) {
      throw new Error('Unavailable');
    }

    const biz = bizcards[0];
    const bizDetails = biz.details[0];
    let details = { ...bizDetails };

    if (bizDetails.amount) {
      const amount = asCurrency(bizDetails.amount);
      details = { ...details, amount };
    }

    return Object.freeze({ ...biz, date: asDateObject(biz.date), details });
  },
});

export default DataService;
