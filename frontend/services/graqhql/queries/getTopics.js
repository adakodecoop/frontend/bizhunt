export default `
  query getTopics {
    topics {
      id
      code
      name
      color
    }
  }
`;
