export const state = () => ({
  selectedTopics: [],
  selectedTypes: [],
  selectedGeofocus: [],
});

export const mutations = {
  SET_SELECTED_TOPICS: (state, topics) => (state.selectedTopics = topics),
  SET_SELECTED_TYPES: (state, types) => (state.selectedTypes = types),
  SET_SELECTED_GEOFOCUS: (state, geofocus) => {
    state.selectedGeofocus = geofocus;
  },
};

export const actions = {
  setSelectedTopics: ({ commit }, topics) => {
    commit('SET_SELECTED_TOPICS', topics);
  },
  setSelectedTypes: ({ commit }, types) => {
    commit('SET_SELECTED_TYPES', types);
  },
  setSelectedGeofocus: ({ commit }, geofocus) => {
    commit('SET_SELECTED_GEOFOCUS', geofocus);
  },
};

export const getters = {
  getSelectedTopics: (state) => state.selectedTopics,
  getSelectedTypes: (state) => state.selectedTypes,
  getSelectedGeofocus: (state) => state.selectedGeofocus,
  getSelectedFilters: (state, getters) => ({
    topics: getters.getSelectedTopics,
    types: getters.getSelectedTypes,
    geofocus: getters.getSelectedGeofocus,
  }),
  getActiveTopics: (state, getters, rootState, rootGetters) => {
    const bizs = rootGetters['bizcards/getBizcards'];
    const activeTopics = bizs
      .flatMap((bizcard) => bizcard.topics)
      .map((topic) => topic.code);

    return new Set(activeTopics);
  },
  getActiveTypes: (state, getters, rootState, rootGetters) => {
    const bizs = rootGetters['bizcards/getBizcards'];
    const activeTypes = bizs.map((bizcard) => bizcard.type.code);

    return new Set(activeTypes);
  },
  getActiveGeofocus: (state, getters, rootState, rootGetters) => {
    const bizs = rootGetters['bizcards/getBizcards'];
    const activeGeofocus = bizs.map((bizcard) => bizcard.geofocus.code);

    return new Set(activeGeofocus);
  },
};
