export default `
  query getBizcard($slug: String!) {
    bizcards(where: { slug: $slug }) {
      id
      slug
      title
      source
      date
      topics {
        code
        name
        color
      }
      type {
        code
        color
      }
      details {
        __typename
        ... on ComponentBizcardBizcall {
          elegibles
          amount
          link
          description
          notes
          attachments {
            url
          }
        }
        ... on ComponentBizcardBizevent {
          venue
          link
          description
          notes
          attachments {
            url
          }
        }
        ... on ComponentBizcardBizmisc {
          link
          description
          notes
          attachments {
            url
          }
        }
      }
    }
  }
`;
