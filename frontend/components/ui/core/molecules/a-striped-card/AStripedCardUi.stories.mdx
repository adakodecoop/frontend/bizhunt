import { Meta, Preview, Props, Story } from '@storybook/addon-docs/blocks';
import { DependenciesTable } from 'storybook-addon-deps/blocks';
import { withKnobs, object } from '@storybook/addon-knobs';

import AStripedCardUi from './AStripedCardUi.vue';

<Meta
  title="Core/Molecules/AStripedCardUi"
  component={AStripedCardUi}
  parameters={{ component: AStripedCardUi }}
/>

# A-Striped-Card-Ui

A generic card with a vertical stripe on the left.

<Preview columns={2}>
  <Story name="default" decorators={[withKnobs]}>
    {{
      components: { AStripedCardUi },
      props: {
        styles: {
          default: () => object(
            'CSS Variables',
            {
              '--a-striped-card-ui--min-h': '8rem',
              '--a-striped-card-ui--max-w': '24rem',
              '--a-striped-card-ui--bg-color': '#f4f5f7',
              '--a-striped-card-ui--border-color': 'transparent',
              '--a-striped-card-ui--stripe--bg-color': '#6b7280',
              '--a-striped-card-ui--stripe--text-color': '#ffffff',
            },
            'Styles'
          ),
        },
      },
      template: `
        <a-striped-card-ui :style="styles">
          <a-box-l class="reset p-3">
            <span>Content area</span>
          <a-box-l>
        </a-striped-card-ui>
      `,
    }}
  </Story>
  <Story name="basic-card">
    {{
      components: { AStripedCardUi },
      template: `
        <a-striped-card-ui>
          <a-box-l class="reset p-3">
            <a-stack-l class="reset space-y-1">
              <span class="block text-md leading-tight">
                Leo integer malesuada nunc vel risus commodo viverra quis ipsum suspendisse
              </span>
              <span class="block text-xs leading-tight text-gray-600">
                Ut tristique et egestas quis ipsum suspendisse ultrices
              </span>
              <span class="block text-sm leading-tight">
                Id semper risus in hendrerit gravida rutrum. Diam quam nulla porttitor massa id neque aliquam.
                Feugiat vivamus at augue eget. Dui id ornare arcu odio ut sem. Sit amet est placerat in.
                Elit ut aliquam purus.
              </span>
            </a-stack-l>
          </a-box-l>
        </a-striped-card-ui>
      `,
    }}
  </Story>
  <Story name="full-card">
    {{
      components: { AStripedCardUi },
      template: `
        <a-striped-card-ui>
          <template #stripe>
            <a-box-l class="reset px-2 py-3">
              <a-stack-l class="a-stack-l--splitAfter-1">
                <span class="invisible">Icon</span>
                <time datetime="2020-07-31T00:00:00.000Z" class="text-xs">
                  31/07<br>
                  2020
                </time>
              </a-stack-l>
            </a-box-l>
          </template>
          <template #default>
            <a-box-l class="reset p-3">
              <a-stack-l class="reset space-y-1">
                <span class="block text-md leading-tight">
                  Leo integer malesuada nunc vel risus commodo viverra quis ipsum suspendisse
                </span>
                <span class="block text-xs leading-tight text-gray-600">
                  Ut tristique et egestas quis ipsum suspendisse ultrices
                </span>
                <span class="block text-sm leading-tight">
                  Id semper risus in hendrerit gravida rutrum. Diam quam nulla porttitor massa id neque aliquam.
                  Feugiat vivamus at augue eget. Dui id ornare arcu odio ut sem. Sit amet est placerat in.
                  Elit ut aliquam purus.
                </span>
              </a-stack-l>
            </a-box-l>
          </template>
        </a-striped-card-ui>
      `,
    }}
  </Story>
</Preview>

## API

<Props of={AStripedCardUi} />

## Installation

```bash
yarn add -E @bit/adakode.ak-ui.core.molecules.a-striped-card
```

## Usage

```html
<!-- Full custom Striped Card -->
<a-striped-card-ui>
  <template #stripe>
    <a-box-l class="reset px-2 py-3">
      <a-stack-l class="a-stack-l--splitAfter-1">
        <svg><!-- SGV Icon --></svg>
        <time datetime="2020-07-31T00:00:00.000Z" class="text-xs">
          31/07<br>
          2020
        </time>
      </a-stack-l>
    </a-box-l>
  </template>
  <template #default>
    <a-box-l class="reset p-3">
      <a-stack-l class="reset space-y-1 a-stack-l--splitAfter-2">
        <span class="block text-md leading-tight">
          Leo integer malesuada nunc vel risus commodo viverra quis ipsum suspendisse
        </span>
        <span class="block text-xs leading-tight text-gray-600">
          Ut tristique et egestas quis ipsum suspendisse ultrices
        </span>
        <a-cluster-ui>
          <a-badge-ui class="reset bg-indigo-800 text-white">IoT</a-badge-ui>
          <a-badge-ui class="reset bg-pink-800 text-white">Smart City</a-badge-ui>
        </a-cluster-ui>
      </a-stack-l>
    </a-box-l>
  </template>
</a-striped-card-ui>
```

## Customization

There are several ways to customize the look and feel of the component,
depending on the desired scope of the customization.

### Global Customization

To customize the look and feel of **every single instance** of the component
create a css file defining a class named after the kebab-case version of the component name
and define any of the custom properties below:

| CSS variable                              |   default     | design token                  |
| :---------------------------------------- | ------------: | :---------------------------- |
| `--a-striped-card-ui--min-h`              |    `8rem`     | `theme('spacing.32')`         |
| `--a-striped-card-ui--max-w`              |   `24rem`     | `theme('maxWidth.sm')`        |
| `--a-striped-card-ui--bg-color`           | `#f4f5f7`     | `theme('colors.gray.100')`    |
| `--a-striped-card-ui--border-color`       | `transparent` | `theme('colors.transparent')` |
| `--a-striped-card-ui--stripe--bg-color`   | `#6b7280`     | `theme('colors.gray.500')`    |
| `--a-striped-card-ui--stripe--text-color` | `#ffffff`     | `theme('colors.white')`       |

These are the steps:

```bash
mkdir -p src/assets/styles/components/ui/core/molecules && touch $_/AStripedCardUi.css
```

```css
/* <rootDir>/src/assets/styles/components/index.css */
@import './ui/core/molecules/AStripedCardUi.css';
```

```css
/* <rootDir>/src/assets/styles/components/ui/core/molecules/AStripedCardUi.css */
.a-striped-card-ui {
  --a-striped-card-ui--min-h: theme('spacing.32');
  --a-striped-card-ui--max-w: theme('maxWidth.sm');
  --a-striped-card-ui--bg-color: theme('colors.gray.100');
  --a-striped-card-ui--border-color: theme('colors.transparent');
  --a-striped-card-ui--stripe--bg-color: theme('colors.gray.500');
  --a-striped-card-ui--stripe--text-color: theme('colors.white');
}
```

### Scoped Customization

To customize the look and feel of a **group of instances** of the component
augment the specificity of the component class with a scoping class:

```css
/* <rootDir>/src/assets/styles/components/ui/core/molecules/AStripedCardUi.css */
.a-striped-card-ui.a-striped-card-ui--success {
  --a-striped-card-ui--stripe-bg-color: theme('colors.success');
}
.a-striped-card-ui.a-striped-card-ui--error {
  --a-striped-card-ui--stripe-bg-color: theme('colors.error');
}
```

```html
<!-- <rootDir>/src/components/path/to/the/ConsumerComponent.vue -->
<a-striped-card-ui class="a-striped-card-ui--success">
  <a-box-l>
    <a-center-l>
      <!-- Success message -->
    </a-center-l>
  </a-box-l>
</a-striped-card-ui>

<a-striped-card-ui class="a-striped-card-ui--error">
  <a-box-l>
    <a-center-l>
      <!-- Error message -->
    </a-center-l>
  </a-box-l>
</a-striped-card-ui>
```

### Instance Customization

To customize the look and feel of a **single instance** of the component
you can define the desired CSS variables inline:

```html
<!-- Using CSS variables -->
<a-striped-card-ui
  :style="{
    '--a-striped-card-ui--min-h': '8rem',
    '--a-striped-card-ui--max-w': '24rem',
    '--a-striped-card-ui--bg-color': '#f4f5f7',
    '--a-striped-card-ui--border-color': 'transparent',
    '--a-striped-card-ui--stripe--bg-color': '#6b7280',
    '--a-striped-card-ui--stripe--text-color': '#ffffff',
  }"
>
  <span><!-- Content area --></span>
</a-striped-card-ui>
```

## Dependencies

<DependenciesTable
  titleDependencies="Dependencies"
  titleDependents="Dependents"
  of={AStripedCardUi}
/>

## Links

Source: https://gitlab.com/adakodecoop/coop/adakode-ui/-/tree/master/src/components/ui/core/molecules/AStripedCard  
Bit: https://bit.dev/adakode/ak-ui/core/molecules/a-striped-card
