export default ({ store, route }) => {
  // console.log('[middleware/store-route]', route.fullPath);
  store.dispatch('routing/setFromRoute', route.fullPath);
};
