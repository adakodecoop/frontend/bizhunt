# Informació tècnica

## Disseny

* Per implementar el disseny del frontend de l'aplicació hem utilitzat [Tailwind CSS](https://tailwindcss.com/).
* Els tokens de disseny estan definits al fitxer `tailwind.config.js` que pot trobar-se a l'arrel del projecte.
* Hem utilitzat la font [Inter](https://fonts.google.com/specimen/Inter?query=inter#standard-styles) com a tipografia base de l'aplicació.

## Desenvolupament

Per construir aquesta aplicació hem utilitzat, entre d'altres, les tecnologies següents:

* [Nuxt.js](https://nuxtjs.org/): Framework JavaScript utilitzat per construir el frontend de l'aplicació.
* [Tailwind CSS](https://tailwindcss.com/): Framework CSS utilitzat per implementar el disseny de l'aplicació.
* [Strapi](https://strapi.io/): [Headless CMS](https://bejamas.io/discovery/headless-cms/) utilitzat per gestionar els continguts de l'aplicació.
* [XState](https://xstate.js.org/): Llibreria utilitzada per programar la màquina d'estat encarregada d'enviar el digest setmanal.

### Requeriments mínims

Per contribuir al codi de l'aplicació cal tenir instal·lat:

* [Volta](https://volta.sh/) per gestionar les eines de desenvolupament
* [Node.js](https://nodejs.org/en/) versió 14.15.0 o superior
* [Yarn](https://classic.yarnpkg.com/lang/en/) versió 1.22.0 o superior
* [Git](https://git-scm.com/) per accedir i actualitzar el codi

### Variables d'entorn

Pel correcte funcionamient de l'aplicació és necessari definir les següents variables d'entorn:

#### Frontend

| Variable | Valor en producció | Descripció |
| :--- | :--- | :--- |
| APP_URL | https://bizhunt.citilab.eu | URL del frontend de l'aplicació |
| API_URL | https://bizhunt-cms.citilab.eu | URL del gestor de continguts de l'aplicació |
| STRAPI_URL | https://bizhunt-cms.citilab.eu | URL del gestor de continguts de l'aplicació |

#### Backend

| Variable | Valor en producció | Descripció |
| :--- | :--- | :--- |
| STRAPI_PLUGIN_I18N_INIT_LOCALE_CODE | ca | idioma per defecte dels continguts |
| ADMIN_JWT_SECRET | dyeOJX/NweECZH4lpxYg+DQeWzsfBGCEQV/An4OyYcVh/t9U0XgmfnH8xq7Hd7//zc8+3gDtzr3BokZdDEjFyw | - |
| JWT_SECRET | uNgR4z9kaXCHfpVwt5rDJPGzu7mNnWee6/vJS2K6Iib4k5Z1niO9jPZvgCBE/mysN1J5UVleDrEb9GKG1RnT5g== | - |
| API_URL | https://bizhunt-cms.citilab.eu | URL del gestor de continguts de l'aplicació |
| DATABASE_HOST | 127.0.0.1 | IP del servidor de base de dades |
| DATABASE_PORT | 5432 | Port del servidor de base de dades |
| DATABASE_NAME | bizhunt | Nom de la base de dades de l'aplicació |
| DATABASE_USERNAME | citilab-tic | Usuària d'accés a la base de dades de l'aplicació |
| DATABASE_PASSWORD | j8VKQFte | Contrasenya d'accés a la base de dades de l'aplicació |
| BH_FRONTEND_URL | https://bizhunt.citilab.eu | URL del frontend de l'aplicació |
| BH_USERZERO_USERNAME | dmartinez | Nom del primer usuari a rebre el digest setmanal |
| BH_USERZERO_EMAIL | dmartinez@e-citilab.eu | Adreça de correu del primer usuari a rebre el digest setmanal |
| BH_DIGEST_EMAIL_FROM | dmartinez@e-citilab.eu | Adreça d'enviament del digest setmanal |
| BH_DIGEST_EMAIL_REPLYTO | dmartinez@e-citilab.eu | Adreça de resposta del digest setmanal |
