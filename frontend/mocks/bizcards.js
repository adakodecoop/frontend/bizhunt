import faker from 'faker';
import { graphql } from 'msw';
import { generateSeq } from '@bit/adakode.ak-fns.utils.array';
import { topics, types, geofocus } from './taxonomies';

const sources = [
  'Universitat de Barcelona',
  'Diputació de Barcelona',
  'Generalitat de Catalunya - OSIC',
  'Ajuntament de Barcelona',
];

/** */
const buildBizcard = () => ({
  id: faker.random.uuid(),
  slug: faker.lorem.slug(5),
  title: faker.lorem.sentence(15),
  source: faker.random.arrayElement(sources),
  date: faker.date.between(
    new Date(2020, 5, 17, 0, 0, 0),
    new Date(2020, 9, 1, 0, 0, 0)
  ),
  topics: [faker.random.arrayElement(topics)].map(({ code, name, color }) => ({
    code,
    name,
    color,
  })),
  type: [faker.random.arrayElement(types)]
    .map(({ code, name, color }) => ({
      code,
      name,
      color,
    }))
    .pop(),
  geofocus: [faker.random.arrayElement(geofocus)]
    .map(({ code, name }) => ({
      code,
      name,
    }))
    .pop(),
});

/** */
const bizcards = generateSeq(20).map(buildBizcard);

/** */
const buildBizcardDetails = () => ({
  // link: faker.internet.url(),
  link:
    'https://vercel.com/guides/deploying-nextjs-using-fathom-analytics-with-vercel',
  description: faker.lorem.paragraphs(4),
  notes: faker.lorem.paragraph(4),
  attachments: generateSeq(3).map((n) => ({
    url: `https://www.pareudepararme.org/assets/img/PDP-c${n}-ca.pdf`,
  })),
});

/** */
const bizcallDetails = () => ({
  details: {
    amount: faker.finance.amount(1000, 10000, 2, '€'),
    // elegibles: faker.lorem.words(2),
    elegibles: faker.lorem.words(20),
    ...buildBizcardDetails(),
  },
});

/** */
const bizeventDetails = () => ({
  details: {
    venue: faker.address.streetAddress(),
    ...buildBizcardDetails(),
  },
});

/** */
const bizmiscDetails = () => ({
  details: {
    ...buildBizcardDetails(),
  },
});

/** */
const getBizcard = (slug) => {
  const bizcard = bizcards.find((bizcard) => bizcard.slug === slug);

  if (!bizcard) return null;

  /* prettier-ignore */
  switch (bizcard.type.code) {
    case 'al': return {
      ...bizcard,
      ...bizmiscDetails(),
    };
    case 'cfe': return {
      ...bizcard,
      ...bizeventDetails(),
    };
    default: return {
      ...bizcard,
      ...bizcallDetails(),
    };
  }
};

export default [
  graphql.query('getBizcards', (req, res, ctx) =>
    res(ctx.delay(), ctx.data({ bizcards }))
  ),
  graphql.query('getBizcard', (req, res, ctx) => {
    const { slug } = req.variables;
    const bizcard = getBizcard(slug);
    return res(ctx.delay(), ctx.data({ bizcard }));
  }),
];
