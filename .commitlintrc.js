const czConfig = require("./.commitizenrc.js");

module.exports = {
  extends: ["@commitlint/config-conventional"],
  rules: {
    "type-enum": () => [2, "always", czConfig.types.map((type) => type.value)],
  },
};
