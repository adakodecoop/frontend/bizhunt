import { ApiClient, DataService } from '~/services';

export default ({ $config, app }, inject) => {
  const apiClient = ApiClient.init({
    type: 'graphql',
    options: {
      url: `${$config.apiURL}/graphql`,
      token: () => app.$authService.getToken(),
    },
  });

  inject('dataService', DataService(apiClient));
};
