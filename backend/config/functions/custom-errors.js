'use strict';

const Boom = require('boom');

module.exports = {
  /**
   * @param {String} message
   */
  emitMessage(message) {
    throw Boom.badData(message);
  },
  /**
   *
   * @param {Array} fields
   */
  emitRequired(fields) {
    let message;

    if (fields.length > 1) {
      message = `Els camps ${fields.join(', ')} són obligatoris`;
    } else {
      message = `El camp ${fields[0]} és obligatori`;
    }

    throw Boom.badData(message);
  },
};
