require('dotenv').config();

module.exports = [
  {
    id: 1,
    username: process.env.BH_USERZERO_USERNAME,
    email: process.env.BH_USERZERO_EMAIL,
    provider: 'local',
    confirmed: true,
    blocked: false,
    role: 1,
    created_by: 1,
    updated_by: 1,
  },
];
