import faker from 'faker';
import { graphql } from 'msw';

export const topics = [
  {
    id: faker.random.uuid(),
    code: 'app',
    name: 'Apps',
    /* theme('colors.pink.400') */
    color: '#f17eb8',
  },
  {
    id: faker.random.uuid(),
    code: 'bd',
    name: 'Big Data / Anàlisi de dades',
    /* theme('colors.pink.200') */
    color: '#fad1e8',
  },
  {
    id: faker.random.uuid(),
    code: 'bl',
    name: 'Blockchain',
    /* theme('colors.red.400') */
    color: '#f98080',
  },
  {
    id: faker.random.uuid(),
    code: 'dw',
    name: 'Desenvolupament web',
    /* theme('colors.red.300') */
    color: '#f8b4b4',
  },
  {
    id: faker.random.uuid(),
    code: 'di',
    name: 'Disseny industrial',
    /* theme('colors.orange.400') */
    color: '#ff8a4c',
  },
  {
    id: faker.random.uuid(),
    code: 'ee',
    name: 'Eficiència energètica',
    /* theme('colors.orange.200') */
    color: '#fcd9bd',
  },
  {
    id: faker.random.uuid(),
    code: 'fi',
    name: 'Finances',
    /* theme('colors.yellow.300') */
    color: '#faca15',
  },
  {
    id: faker.random.uuid(),
    code: 'ga',
    name: "Gestió de l'aigua",
    /* theme('colors.yellow.200') */
    color: '#fce96a',
  },
  {
    id: faker.random.uuid(),
    code: 'ia',
    name: 'Intel·ligència artificial',
    /* theme('colors.green.400') */
    color: '#31c48d',
  },
  {
    id: faker.random.uuid(),
    code: 'iot',
    name: 'IoT',
    /* theme('colors.green.200') */
    color: '#bcf0da',
  },
  {
    id: faker.random.uuid(),
    code: 'lo',
    name: 'Logística',
    /* theme('colors.teal.300') */
    color: '#7edce2',
  },
  {
    id: faker.random.uuid(),
    code: 'mk',
    name: 'Marketing digital',
    /* theme('colors.teal.100') */
    color: '#d5f5f6',
  },
  {
    id: faker.random.uuid(),
    code: 'oe',
    name: "Organització d'esdeveniments",
    /* theme('colors.blue.400') */
    color: '#76a9fa',
  },
  {
    id: faker.random.uuid(),
    code: 'sp',
    name: 'Selecció de personal',
    /* theme('colors.blue.200') */
    color: '#c3ddfd',
  },
  {
    id: faker.random.uuid(),
    code: 'sd',
    name: 'Senyalització digital',
    /* theme('colors.indigo.300') */
    color: '#b4c6fc',
  },
  {
    id: faker.random.uuid(),
    code: 'sc',
    name: 'Smart City',
    /* theme('colors.indigo.100') */
    color: '#e5edff',
  },
  {
    id: faker.random.uuid(),
    code: 'vi',
    name: 'Videojocs',
    /* theme('colors.purple.400') */
    color: '#ac94fa',
  },
  {
    id: faker.random.uuid(),
    code: 'xs',
    name: 'Xarxes socials',
    /* theme('colors.purple.200') */
    color: '#dcd7fe',
  },
  {
    id: faker.random.uuid(),
    code: 'a',
    name: 'Altres',
    /* theme('colors.red.100') */
    color: '#fde8e8',
  },
];

export const types = [
  {
    id: faker.random.uuid(),
    code: 'aj',
    name: 'Ajut',
    /* theme('colors.red.700') */
    color: '#c81e1e',
  },
  {
    id: faker.random.uuid(),
    code: 'cs',
    name: 'Cerca de socis',
    /* theme('colors.orange.500') */
    color: '#ff5a1f',
  },
  {
    id: faker.random.uuid(),
    code: 'cfe',
    name: 'Congrés/Fira/Esdeveniment',
    /* theme('colors.yellow.400') */
    color: '#e3a008',
  },
  {
    id: faker.random.uuid(),
    code: 'li',
    name: 'Licitació',
    /* theme('colors.green.700') */
    color: '#046c4e',
  },
  {
    id: faker.random.uuid(),
    code: 'oc',
    name: 'Oferta comercial',
    /* theme('colors.blue.800') */
    color: '#1e429f',
  },
  {
    id: faker.random.uuid(),
    code: 'pr',
    name: 'Premi',
    /* theme('colors.indigo.500') */
    color: '#6875f5',
  },
  {
    id: faker.random.uuid(),
    code: 'su',
    name: 'Subvenció',
    /* theme('colors.purple.700') */
    color: '#6c2bd9',
  },
  {
    id: faker.random.uuid(),
    code: 'al',
    name: 'Altres',
    /* theme('colors.pink.600') */
    color: '#d61f69',
  },
];

export const geofocus = [
  {
    id: faker.random.uuid(),
    code: 'EU',
    name: 'Europeu',
    /* theme('colors.gray.500') */
    color: '#6b7280',
  },
  {
    id: faker.random.uuid(),
    code: 'ES',
    name: 'Estatal',
    /* theme('colors.gray.700') */
    color: '#374151',
  },
  {
    id: faker.random.uuid(),
    code: 'CA',
    name: 'Autonòmic',
    /* theme('colors.gray.500') */
    color: '#6b7280',
  },
  {
    id: faker.random.uuid(),
    code: 'Pro',
    name: 'Provincial',
    /* theme('colors.gray.700') */
    color: '#374151',
  },
  {
    id: faker.random.uuid(),
    code: 'Loc',
    name: 'Local',
    /* theme('colors.gray.500') */
    color: '#6b7280',
  },
];

export default [
  graphql.query('getTopics', (req, res, ctx) =>
    res(ctx.delay(), ctx.data({ topics }))
  ),
  graphql.query('getTypes', (req, res, ctx) =>
    res(ctx.delay(), ctx.data({ types }))
  ),
  graphql.query('getGeofocus', (req, res, ctx) =>
    res(ctx.delay(), ctx.data({ geofocus }))
  ),
];
