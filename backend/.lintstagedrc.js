module.exports = {
  '*.js': ['eslint --fix'],
  '*.{json,yml,yaml,html,md}': ['prettier --write'],
};
